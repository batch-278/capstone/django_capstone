from django import forms

class LoginForm(forms.Form):
    username = forms.CharField(label="username", max_length=20, required=True)
    password = forms.CharField(label="password", max_length=20, required=True)

class RegistrationForm(forms.Form):
    username = forms.CharField(label="username", max_length=20, required=True)
    password = forms.CharField(label="password", max_length=20, required=True)
    confirm_password = forms.CharField(label="confirm_password", max_length=20, required=True)
    first_name = forms.CharField(label="first_name", max_length=20, required=True)
    last_name = forms.CharField(label="last_name", max_length=20, required=True)
    email = forms.CharField(label="email", max_length=20, required=True)
    
class AddTaskForm(forms.Form):
    task_name = forms.CharField(label="task_name", max_length=50, required=True)
    description = forms.CharField(label="description", max_length=500, required=True)

class AddEventForm(forms.Form):
    event_name = forms.CharField(label="event_name", max_length=50, required=True)
    description = forms.CharField(label="description", max_length=500, required=True)

class UpdateTaskForm(forms.Form):
    task_name = forms.CharField(label = "task_name", max_length=50)
    description = forms.CharField(label = "description", max_length=500)
    status = forms.CharField(label = "status", max_length=50)

class UpdateEventForm(forms.Form):
    event_name = forms.CharField(label = "event_name", max_length=50)
    description = forms.CharField(label = "description", max_length=500)
    status = forms.CharField(label = "status", max_length=50)

class UpdateUserForm(forms.Form):
    first_name = forms.CharField(label = "first_name", max_length=50, required=True)
    last_name = forms.CharField(label = "last_name", max_length=50, required=True)
    password = forms.CharField(label = "password", max_length=50, required=True)
    confirm_password = forms.CharField(label="confirm_password", max_length=20, required=True)